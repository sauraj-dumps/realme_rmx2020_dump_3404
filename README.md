## RMX2020-user 10 QP1A.190711.020 1580942954 release-keys
- Manufacturer: realme
- Platform: mt6768
- Codename: RMX2020
- Brand: realme
- Flavor: cipher_RMX2020-userdebug
- Release Version: 12
- Id: SP2A.220405.004
- Incremental: eng.neolit.20220410.012152
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/coral/coral:12/SP2A.220405.003/8210211:user/release-keys
- OTA version: 
- Branch: RMX2020-user-10-QP1A.190711.020-1580942954-release-keys
- Repo: realme_rmx2020_dump_3404


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
